function fetchRandomNumbers() {
  return new Promise((resolve, reject) => {
    console.log("Fetching number...");
    setTimeout(() => {
      let randomNum = Math.floor(Math.random() * (100 - 0 + 1)) + 0;
      console.log("Received random number:", randomNum);
      resolve(randomNum);
    }, (Math.floor(Math.random() * 5) + 1) * 1000);
  });
}

// Task 1 - Promise
fetchRandomNumbers()
.then((randomNum) => console.log(randomNum))
.catch((error) => console.error(error));

// Task 1 - Async/Await
async function task() {
  try {
    const value = await fetchRandomNumbers();
    console.log(value);
  } catch (error) {
    console.error(error);
  }
}
// task();

function fetchRandomString() {
  return new Promise((resolve, reject) => {
    console.log("Fetching string...");
    setTimeout(() => {
      let result = "";
      let characters =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
      let charactersLength = characters.length;
      for (let i = 0; i < 5; i++) {
        result += characters.charAt(
          Math.floor(Math.random() * charactersLength)
        );
      }
      console.log("Received random string:", result);
      resolve(result);
    }, (Math.floor(Math.random() * 5) + 1) * 1000);
  });
}

//Task 1 - Promise
fetchRandomString()
.then((data) =>  console.log(data))
.catch((error) => console.error(error));

//Task 1 - Async/Await
async function task1() {
  try {
    const value = await fetchRandomString();
    console.log(value);
  } catch (error) {
    console.error(error);
  }
}
// task1();

// Task 2
function task2() {
  fetchRandomNumbers()
    .then((data) => {
      let sum = 0;
      sum += data;
      console.log(sum, "sum");
      fetchRandomNumbers()
        .then((data) => {
          sum += data;
          console.log(sum, "sum");
        })
        .catch((error) => console.error(error));
    })
    .catch((error) => console.error(error));
}
// task2();

//Task 2 - Async/Await
async function task2() {
  try {
    let sum = 0;
    const value1 = await fetchRandomNumbers();
    sum += value1;
    console.log(sum, "sum");
    const value2 = await fetchRandomNumbers();
    sum += value2;
    console.log(sum, "sum");
  } catch (error) {
    console.error(error);
  }
}
// task2();

//Task 3
function task3() {
  Promise.all([fetchRandomNumbers(), fetchRandomString()])
    .then((arr) => console.log(arr[0] + arr[1]))
    .catch((error) => console.error(error));
}
// task3();

// Task 3 - Async/Await
async function task3() {
  try {
    let output;
    const arr = await Promise.all([fetchRandomNumbers(), fetchRandomString()]);
    console.log(arr[0] + arr[1], "output");
  } catch (error) {
    console.error(error);
  }
}
// task3();

//Task 4
function task4() {
  let arr = [];
  for (let index = 1; index <= 10; index++) {
  arr.push(fetchRandomNumbers());
  }
  Promise.all(arr)
  .then((array) => array.reduce((acc,cv) => acc+cv,0))
  .then((result) => console.log(result))
  .catch((error) => console.error(error))
}
// task4();

// Task 4 - Async/Await
async function task4() {
  try {
    let arr = [];
    for (let index = 1; index <= 10; index++) {
      arr.push(fetchRandomNumbers());
    }
    let resolvedArr = await Promise.all(arr);
    let sum = resolvedArr.reduce((acc,cv) =>acc+cv, 0);
    console.log(sum, 'sum');
  } catch (error) {
    console.error(error);
  }
}
// task4();
